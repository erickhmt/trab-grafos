// DIJKSTRA

#include <iostream>
#include <list>
#include <queue>
#define TAM 10000000

using namespace std;

class Grafo {
	private:
	int vertice;
	list<pair<int, int> > * adj;

public:

	Grafo(int vertice) {
		this->vertice = vertice; // atribui o numero de vertices

		adj = new list<pair<int, int> >[vertice];
	}

	// adiciona uma aresta ao grafo de v1 a v2
	void add(int v1, int v2, int custo) {
		adj[v1].push_back(make_pair(v2, custo));
	}

	// Dijkstra
	int dijkstra(int orig, int dest) {
	
		int distancia[vertice];
		int visitados[vertice];

		priority_queue < pair<int, int>,
					   vector<pair<int, int> >, greater<pair<int, int> > > priorityQueue;

		for(int i = 0; i < vertice; i++) {
			distancia[i] = TAM;
			visitados[i] = false;
		}

		distancia[orig] = 0;

		// insere na fila
		priorityQueue.push(make_pair(distancia[orig], orig));

		while(!priorityQueue.empty()) {
			pair<int, int> p = priorityQueue.top(); // extrai o pair do topo
			int u = p.second; 
			priorityQueue.pop(); // remove da fila

			if(visitados[u] == false) {
				visitados[u] = true;

				list<pair<int, int> >::iterator it;

				for(it = adj[u].begin(); it != adj[u].end(); it++) {
					int v = it->first;
					int custo_aresta = it->second;

					// relaxamento (u, v)
					if(distancia[v] > (distancia[u] + custo_aresta)) {
						distancia[v] = distancia[u] + custo_aresta;
						priorityQueue.push(make_pair(distancia[v], v));
					}
				}
			}
		}

		// retorna a distancia minima
		return distancia[dest];
	}
};

int main(int argc, char *argv[]){
	Grafo grafo(5);

	grafo.add(0, 1, 4);
	grafo.add(0, 2, 2);
	grafo.add(0, 3, 5);
	grafo.add(1, 4, 1);
	grafo.add(2, 1, 1);
	grafo.add(2, 3, 2);
	grafo.add(2, 4, 1);
	grafo.add(3, 4, 1);

	cout << grafo.dijkstra(0, 4) << endl;

	return 0;
}
