// Busca em profundidade

#include <iostream>
#include <list>
#include <algorithm> 
#include <stack> 
 
using namespace std;
 
class Grafo {
	int vertices; 
	list<int> *adjacente; 
 
public:
	Grafo(int V); //construtor
	void add(int v1, int v2); // adiciona uma aresta
 
	// funcao da busca em largura
	void busca_em_largura(int v);
};
 
Grafo::Grafo(int V) {
	this->vertices = V; // atribui o numero de vertices
	adjacente = new list<int>[vertices]; // cria as listas
}
 
void Grafo::add(int v1, int v2) {
	// adiciona vertice v2 a lista de vertices adjacentes de v1
	adjacente[v1].push_back(v2);
}
 
void Grafo::busca_em_largura(int v) {
	stack<int> pilha;
	bool visitados[vertices]; // visitados
 
	// nao visitados
	for(int i = 0; i < vertices; i++)
		visitados[i] = false;
 
	while(true) {
		if(!visitados[v]) {
			cout << "Visitando vertice " << v << " ...\n";
			visitados[v] = true; 
			pilha.push(v);
		}
 
		bool achou = false;
		list<int>::iterator it;
 
		// busca nao visitado
		for(it = adjacente[v].begin(); it != adjacente[v].end(); it++) {
			if(!visitados[*it]) {
				achou = true;
				break;
			}
		}
 
		if(achou) {
			v = *it; // atualiza o valor	
		} else {	
			pilha.pop();
			
			if(pilha.empty())
				break;
			v = pilha.top();
		}
	}
}
 
int main() {
	int vertices = 8;
 
	Grafo grafo(vertices);
 
	// adicionando as arestas
	grafo.add(0, 1);
	grafo.add(0, 2);
	grafo.add(1, 3);
	grafo.add(1, 4);
	grafo.add(2, 5);
	grafo.add(2, 6);
	grafo.add(6, 7);
	
	grafo.busca_em_largura(0);
 
	return 0;
}
